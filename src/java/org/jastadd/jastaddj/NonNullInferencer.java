package org.jastadd.jastaddj;

import AST.*;

import java.util.*;
import java.io.*;

public class NonNullInferencer extends Frontend {

  /**
   * Default entry point.
   * @param args command-line arguments
   */
  public static void main(String args[]) {
    int exitCode = new NonNullInferencer().run(args);
    if (exitCode != 0) {
      System.exit(exitCode);
    }
  }

  private final JavaParser parser;
  private final BytecodeParser bytecodeParser;

  public NonNullInferencer() {
    super("NonNullInferencer", "1.0.0");
    parser = new JavaParser() {
      @Override
      public CompilationUnit parse(java.io.InputStream is,
          String fileName) throws java.io.IOException, beaver.Parser.Exception {
        return new parser.JavaParser().parse(is, fileName);
      }
    };
    bytecodeParser = new BytecodeParser();
  }

  public int run(String[] args) {
    int exitCode = run(args, bytecodeParser, parser);

    if (exitCode != Frontend.EXIT_SUCCESS) {
      System.err.println("Warning: semantic errors exist - inferred output may not be correct!");
    }

    program.updateRemoteAttributeCollectionsFrontend();
    Options options = program.options();
    if (options.hasOption("-test")) {
      program.emitTest();
    } else {
      program.emit();
    }

    if (options.hasValueForOption("-debugCU")) {
      String value = options.getValueForOption("-debugCU");
      try {
        String fileName = new File(value).getCanonicalPath();
        for(Iterator iter = program.compilationUnitIterator(); iter.hasNext(); ) {
          CompilationUnit unit = (CompilationUnit)iter.next();
          if(unit.relativeName() != null && fileName.equals(new File(unit.relativeName()).getCanonicalPath())) {
            unit.printDebugInfo();
            return exitCode;
          }
        }
      } catch (IOException e) {
      }
      System.err.println("Warning: could not find compilation unit to debug: " + value);
    }

    return exitCode;
  }

  protected void initOptions() {
    super.initOptions();
    Options options = program.options();
    options.addKeyOption("-test");
    options.addKeyValueOption("-import");
    options.addKeyOption("-legacysyntax");
    options.addKeyOption("-disableraw");
    options.addKeyOption("-defaultnonnull");
    options.addKeyValueOption("-debugCU");// debug a compilation unit
  }
  
  protected int processArgs(String[] args) {
    Options options = program.options();
    options.addOptions(args);
    if (!options.hasOption("-d")) {
      options.addOptions(new String[] { "-d", "inferred" });
	}
    program.rawEnabled = !options.hasOption("-disableraw");
	return Frontend.EXIT_SUCCESS;
  }

  protected void printUsage() {
      printVersion();
      System.out.println(
          "\n" + name() + "\n\n" +
          "Usage: java -jar JavaNonNullInferencer.jar <options> <source files>\n" +
          "  -verbose                  Output messages about what the compiler is doing\n" +
          "  -classpath <path>         Specify where to find user class files\n" +
          "  -sourcepath <path>        Specify where to find input source files\n" + 
          "  -bootclasspath <path>     Override location of bootstrap class files\n" + 
          "  -extdirs <dirs>           Override location of installed extensions\n" +
          "  -d <directory>            Specify where to place source files with inferred annotations\n" +
          "                            The default location is a folder named 'inferred'\n" +
          "  -test                     Emit inferred source files to standard output rather than a file\n" +
          "  -import <package>         Include the specified import in the generated source file unless\n" +
          "                            it already exists\n" +
          "  -legacysyntax             Use legacy syntax for inferred annotations, i.e., /*@NonNull*/\n" +
          "  -disableraw               Disable raw types for partially initialised objects. This has\n" +
          "                            the effect that all fields are considered possibly-null\n" +
          "  -help                     Print a synopsis of standard options\n" +
          "  -version                  Print version information\n"
          );
    }


}
